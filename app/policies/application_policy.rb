class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def owner?
    @user == @record.user
  end

  def user?
    @user.role == 'user'
  end

  def manager?
    @user.role == 'manager'
  end

  def admin?
    @user.role == 'admin'
  end
end
