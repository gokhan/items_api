class Api::UsersController < ApplicationController
  before_action :authenticate_user, except: :create

  def index
    run User::Index do |result|
        return render( json: UserRepresenter.for_collection.new(result["model"]) )
    end

    render json: { error: 'Unauthorized access' }, status: 422
  end

  def me
    return render(json:  current_user )
  end

  def create
    run User::Create do |result|
      return render(json: UserRepresenter.new(result["model"]))
    end
    render json: {error: breach_or_form_message(result, @form)}
  end

  def update
    run User::Update do |result|
      return render(json: UserRepresenter.new(result["model"]))
    end

    render json: {error: breach_or_form_message(result, @form)}
  end

  def destroy
    run User::Destroy do |result|
      return render(json: {})
    end

    render json: {}
  end
end
