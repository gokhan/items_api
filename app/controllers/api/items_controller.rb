class Api::ItemsController < ApplicationController
  before_action :authenticate_user

  def index
    run Item::Index do |result|
      return render( json: ItemRepresenter.for_collection.new(result["model"]) )
    end

    render json: {error: 'Unauthorized access'}, status: 422
  end

  def search
    run Item::Search do |result|
      return render( json: ItemRepresenter.for_collection.new(result["model"]) )
    end

    render json: {error: 'Unauthorized access'}, status: 422
  end

  def create
    run Item::Create do |result|
      return render(json: result["model"])
    end

    render json: {error: @form.errors.messages}
  end

  def update
    run Item::Update do |result|
      return render(json: result["model"])
    end

    render json: {error: breach_or_form_message(result, @form)}
  end

  def destroy
    run Item::Destroy do |result|
      return render(json: {})
    end

    render json: {}
  end
end
