class Item < ApplicationRecord
  belongs_to :user
  has_one :location, dependent: :destroy
  acts_as_taggable
  
  include PgSearch  # full text search
  pg_search_scope :search_for, against: :name,
                  :using => {:tsearch => {:prefix => true, :dictionary => "english"} }

end
