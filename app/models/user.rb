class User < ApplicationRecord
  has_secure_password
  has_many :items, dependent: :delete_all
end
