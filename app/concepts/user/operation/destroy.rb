class User::Destroy < Trailblazer::Operation
  step Model( User, :find_by)
  step Policy::Pundit( User::Policy, :destroy? )
  step :destroy!

  def destroy!(options, **)
    options["model"].destroy
  end
end
