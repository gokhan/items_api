require 'reform/form/dry'

class User::Update < Trailblazer::Operation
	extend Contract::DSL
	contract do
		feature Reform::Form::Dry
		property :email
		property :password

		validation :default do
			configure do
				config.messages_file = 'config/locales/error_messages.yml'

		     def email?(value)
		       (value =~ /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i)
		     end

				 def unique?(value)
					 User.where.not(id: form.model.id).find_by(email: value).nil?
				 end
		   end

			required(:email, &:email?)
		end
		validation :unique, inherit: true do
			configure do
				option :form
				config.messages_file = 'config/locales/error_messages.yml'

				def unique?(value)
					User.where.not(id: form.model.id).find_by(email: value).nil?
				end
			end

		  required(:email, &:unique?)
		end
	end



	step  Model(User, :find)
	step  Policy::Pundit( User::Policy, :update? )
  step 	Contract::Build()
  step  Contract::Validate(key: "user")
	step  :save!
	#step	Contract::Persist()

	def save!(options, model:, params:, **)
		model.email = params["user"]["email"]
		model.password = params["user"]["password"] unless params.fetch("user", {}).fetch("password", nil).blank?
		model.save
	end

	# def model!(options, params:, **)
	# 	options["model"] = User.find_by(id: params["id"])
	# end

end
