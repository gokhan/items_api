class User::Index < Trailblazer::Operation
  step Policy::Pundit( User::Policy, :index? )
  step :list!

  def list!(options, *)
    options["model"] = User.all
  end
end
