require 'representable/json'

class UserRepresenter < Representable::Decorator
  include Representable::JSON

  property :id
  property :email
end
