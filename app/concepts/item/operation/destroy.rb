class Item::Destroy < Trailblazer::Operation
  step Model( Item, :find_by)
  step Policy::Pundit( Item::Policy, :destroy? )
  step :destroy!

  def destroy!(options, **)
    options["model"].destroy
  end
end
