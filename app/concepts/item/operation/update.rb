class Item::Update < Trailblazer::Operation
  class Present < Trailblazer::Operation
    step Model(Item, :find_by)
    step Contract::Build( constant: Item::Contract::Create )
  end

  step Nested( Present )
  step Contract::Validate( key: :item )
  step Contract::Persist( )
end
