class Item::Create < Trailblazer::Operation
  class Present < Trailblazer::Operation
    step Model(Item, :new)
		step  :assign_user
    step Contract::Build( constant: Item::Contract::Create )
		def assign_user(options, current_user:, **)
			options["model"].user = current_user
		end
  end

  step Nested( Present )
  step Contract::Validate( key: :item )
  step Contract::Persist( )

end
