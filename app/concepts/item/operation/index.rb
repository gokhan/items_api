class Item::Index < Trailblazer::Operation
  step :list!

  def list!(options, current_user:, **)
    options["model"] = current_user.items.order(:id)
  end
end
