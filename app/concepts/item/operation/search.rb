class Item::Search < Trailblazer::Operation
  step :list!

  def list!(options, current_user:, params:, **)
    options["model"] = current_user.items.search_for(params[:keyword]).order(:id)
  end

end
