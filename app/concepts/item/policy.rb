class Item::Policy < ApplicationPolicy
  def index?
    @user.present?
  end

  def update?
    owner? 
  end

  def destroy?
    update?
  end
end
