require 'representable/json'

class ItemRepresenter < Representable::Decorator
  include Representable::JSON

  property :id
  property :name
end
