class CreateLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :locations do |t|
      t.references :item, foreign_key: true
      t.decimal :lgn, precision: 10, scale: 6
      t.decimal :lat, precision: 10, scale: 6

      t.timestamps
    end
  end
end
