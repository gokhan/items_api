# create our users for different roles
%w(user manager admin).each do |role|
  User.create email: "#{role}@example.com", password: role, role: role
end
