module TokenHelpers
  def json_response
      ActiveSupport::JSON.decode @response.body
  end

  def header_for(type, user: Fabricate(type))
    token = Knock::AuthToken.new(payload: { sub: user.id }).token

    {
      'Authorization': "Bearer #{token}"
    }
  end
end
