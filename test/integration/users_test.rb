require 'test_helper'

class UsersTest < ActionDispatch::IntegrationTest
  include TokenHelpers

  # authorization
  def test_authorized_personel_can_list_users
    [:manager, :admin].each do |role|
      get api_users_url, headers: header_for(role)

      assert_response :success
    end
  end

  def test_unauthorized_personel_can_not_see_users
    get api_users_url, headers: header_for(:user)

    assert_response :unprocessable_entity
  end


  # creation
  def test_create_successfully
    post api_users_url, params: {"user" => {"email" => "test@test.com", "password"=> "12345678" }}

    assert_nil json_response["error"]
    assert_response :success
  end

  def test_create_fails_with_not_unique_email
    user = Fabricate(:user, email: 'user@email.com')
    post api_users_url, params: {"user" => {"email" => user.email, "password"=> "12345678" }}
    assert_not_nil json_response["error"]
    assert_equal ({"email"=>["is not unique"]}), json_response["error"]
    assert_response :success
  end

  def test_create_fails_with_invalid_email
    post api_users_url, params: {"user" => {"email" => "testme.com", "password"=> "12345678" }}
    assert_not_nil json_response["error"]
    assert_equal ({"email"=>["is not valid email"]}), json_response["error"]
    assert_response :success
  end

  # update
  def test_update_email
    user = Fabricate(:user)
    patch api_user_url(user),
          params: {"user" => {"email" => "updated@test.com", "password": ""}},
          headers: header_for(:user, user: user)

    assert_equal "updated@test.com", User.first.email
    assert_response :success
  end

  def test_update_email_and_password
    user = Fabricate(:user)
    patch api_user_url(user),
          params: {"user" => {"email" => "updated@test.com", "password": "new_one"}},
          headers: header_for(:user, user: user)

    assert_equal "updated@test.com", User.first.email
    assert_response :success
  end

  def test_update_email_using_current_email
    user = Fabricate(:user)
    patch api_user_url(user),
          params: {"user" => {"email" => user.email, "password": "new_one"}},
          headers: header_for(:user, user: user)

    assert_nil json_response["error"]
    assert_response :success
  end

  def test_update_email_not_unique
    user = Fabricate(:user)
    manager = Fabricate(:manager)
    patch api_user_url(user),
          params: {"user" => {"email" => manager.email}},
          headers: header_for(:user, user: user)

    assert_not_nil json_response["error"]
    assert_equal ({"email"=>["is not unique"]}), json_response["error"]
    assert_response :success
  end

  def test_update_email_not_unique
    user = Fabricate(:user)
    patch api_user_url(user),
          params: {"user" => {"email" => "what"}},
          headers: header_for(:user, user: user)

    assert_not_nil json_response["error"]
    assert_equal ({"email"=>["is not valid email"]}), json_response["error"]
    assert_response :success
  end

  # destroy
  def test_destroy_success
    user = Fabricate(:user)

    delete api_user_url(user),
          headers: header_for(:admin)

    assert_equal 1, User.count
    assert_response :success
  end


  def test_destroy_fail
    user = Fabricate(:user)
    another = Fabricate(:user)
    delete api_user_url(user),
          headers: header_for(:user, user: another)

    user.reload
    assert_equal 2, User.count
    assert_response :success
  end
end
