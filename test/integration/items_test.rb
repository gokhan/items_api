require 'test_helper'

class ItemsTest < ActionDispatch::IntegrationTest
  include TokenHelpers

  # creation
  def test_create_successfully

    post api_items_url, params: {"item" => {"name" => "Chiang Mai"}},
                            headers: header_for(:user)


    item = Item.first
    assert_nil json_response["error"]
    assert_equal "Chiang Mai", item.name
    assert_response :success
  end

  # create with tags
  def test_create_successfully
    tags = ['date', 'chill', 'cappucciono']
    post api_items_url, params: {"item" => {"name" => "Chiang Mai", tag_list: tags}},
                            headers: header_for(:user)


    item = Item.first
    assert_nil json_response["error"]
    assert_equal "Chiang Mai", item.name
    assert_equal item.tag_list, tags
    assert_response :success
  end



  # index
  def test_index_user_items
    user = Fabricate(:user)
    item = Fabricate(:item, user: user)

    get api_items_url, params: {'x':'x'}, headers: header_for(:user, user: user)

    assert_equal 1, json_response.count

    assert_response :success
  end

  # update
  def test_update_successfully_by_user

    user = Fabricate(:user)
    item = Fabricate(:item, user: user)

    patch api_item_url(item), params: { "item" => {"name": 'Sweet Hoome'} },
                            headers: header_for(:user, user: user)

    item.reload
    assert_equal "Sweet Hoome", item.name

    assert_response :success
  end



  # destroy
  def test_destroy_success
    user = Fabricate(:user)
    item = Fabricate(:item, user: user)

    delete api_item_url(item),
          headers: header_for(:user, user: user)

    assert_equal 0, Item.count
    assert_response :success
  end


  def test_destroy_fail
    user = Fabricate(:user)
    item = Fabricate(:item, user: user)

    delete api_item_url(item),
          headers: header_for(:manager)

    assert_equal 1, Item.count
    assert_response :success
  end

  # search
  def test_item_successfull_search_by_user
    user = Fabricate(:user)
    item = Fabricate(:item, name: 'First', user: user)
    item = Fabricate(:item, name: 'Second', user: user)

    get search_api_items_url(), params: {keyword: 'First'},
          headers: header_for(:user, user: user)

    assert_equal 1, json_response.count
    assert_equal 'First', json_response.first["name"]
    assert_response :success
  end

end
